/**
 * @file rfm69.c
 * @brief RFM69 and RFM69HW library for sending and receiving packets
 *
 * This is a protocol and hardware agnostic driver library for handling HopeRF's RFM69 433/868/915 MHz RF modules.
 * Support is also available for the +20 dBm high power modules called RFM69HW/RFM69HCW.
 *
 * A CSMA/CA (carrier sense multiple access) algorithm can be enabled to avoid collisions.
 * If you want to enable CSMA, you should initialize the random number generator before.
 *
 * This work is based on RFM69-STM32 library by André Heßling https://github.com/ahessling/RFM69-STM32
 *
 * @date Apr 17, 2020
 * @author Ilias Daradimos
 */

#include <rfm69_registers.h>
#include "rfm69.h"
#include <stdlib.h>

#define TIMEOUT_MODE_READY    100 ///< Maximum amount of time until mode switch [ms]
#define TIMEOUT_PACKET_SENT   100 ///< Maximum amount of time until packet must be sent [ms]
#define TIMEOUT_CSMA_READY    500 ///< Maximum CSMA wait time for channel free detection [ms]
#define CSMA_RSSI_THRESHOLD   -85 ///< If RSSI value is smaller than this, consider channel as free [dBm]

void rfm69_clear_FIFO(struct rfm69_dev *device);
void rfm69_waitForModeReady(struct rfm69_dev *device);
void rfm69_sleep(struct rfm69_dev *device);
void rfm69_waitForPacketSent(struct rfm69_dev *device);
void rfm69_writeRegister(struct rfm69_dev *device, uint8_t reg, uint8_t value);
void rfm69_setHighPowerSettings(struct rfm69_dev *device, bool enable);
void rfm69_setPASettings(struct rfm69_dev *device, char forcePA);
uint8_t rfm69_readRegister(struct rfm69_dev *device, uint8_t reg);
void rfm69_wait_for_free_channel(struct rfm69_dev *device);

/**
 *
 * @param device
 * @param reg
 * @return Value
 */
uint8_t rfm69_readRegister(struct rfm69_dev *device, uint8_t reg) {
	uint8_t value;
	device->chip_select(true);
	device->write(&reg, 1);
	device->read(&value, 1);
	device->chip_select(false);
	return value;
}

/**
 *
 * @param device
 * @param reg
 * @param value
 */
void rfm69_writeRegister(struct rfm69_dev *device, uint8_t reg, uint8_t value) {
	device->chip_select(true);
	reg |= 0x80;
	device->write(&reg, 1);
	device->write(&value, 1);
	device->chip_select(false);
}

/**
 * Initialize the RFM69 module.
 * A base configuration is set and the module is put in standby mode.
 *
 * @param device
 * @return Always true
 */
uint8_t rfm69_init(struct rfm69_dev *device) {
	// set base configuration
	if (device->conf_size > 0)
		rfm69_setCustomConfig(device, device->conf, device->conf_size);

	// set PA and OCP settings according to RF module (normal/high power)
	rfm69_setPASettings(device, 0);
	rfm69_setMode(device, RFM69_MODE_STANDBY);
	// Read settings
	device->packet_format = rfm69_readRegister(device, REG_PACKETCONFIG1) >> 7;
	return 1;
}

/**
 * Reset the RFM69 module using the external reset line
 * @param device
 */
void rfm69_reset(struct rfm69_dev *device) {
	device->init = false;
	device->chip_reset(true);
	device->delay_ms(1);
	device->chip_reset(false);
	device->delay_ms(10);
	device->mode = RFM69_MODE_STANDBY;
}

/**
 * Reconfigure the RFM69 module by writing multiple registers at once.
 *
 * @param device
 * @param config Array of register/value tuples
 * @param length Number of elements in config array
 */
void rfm69_setCustomConfig(struct rfm69_dev *device, uint8_t *config,
		uint8_t length) {
	for (uint32_t i = 0; i < length; i++) {
		rfm69_writeRegister(device, *(config + i * 2), *(config + i * 2 + 1));
	}
}

/**
 * Enable/disable the power amplifier(s) of the RFM69 module.
 *
 * PA0 for regular devices is enabled and PA1 is used for high power devices (default).
 *
 * @note Use this function if you want to manually override the PA settings.
 * @note PA0 can only be used with regular devices (not the high power ones!)
 * @note PA1 and PA2 can only be used with high power devices (not the regular ones!)
 *
 * @param device rfm69 device
 * @param forcePA If this is 0, default values are used. Otherwise, PA settings are forced.
 *                0x01 for PA0, 0x02 for PA1, 0x04 for PA2, 0x08 for +20 dBm high power settings.
 */
void rfm69_setPASettings(struct rfm69_dev *device, char forcePA) {
	// disable OCP for high power devices, enable otherwise
	rfm69_writeRegister(device, 0x13,
			0x0A | (device->is_high_power ? 0x00 : 0x10));

	if (0 == forcePA) {
		if (device->is_high_power > 0) {
			// enable PA1 only
			rfm69_writeRegister(device, 0x11,
					(rfm69_readRegister(device, 0x11) & 0x1F) | 0x40);
		} else {
			// enable PA0 only
			rfm69_writeRegister(device, 0x11,
					(rfm69_readRegister(device, 0x11) & 0x1F) | 0x80);
		}
	} else {
		// PA settings forced
		char pa = 0;

		if (forcePA & 0x01)
			pa |= 0x80;

		if (forcePA & 0x02)
			pa |= 0x40;

		if (forcePA & 0x04)
			pa |= 0x20;

		// check if high power settings are forced
		rfm69_setHighPowerSettings(device, (forcePA & 0x08) ? 1 : 0);

		rfm69_writeRegister(device, 0x11,
				(rfm69_readRegister(device, 0x11) & 0x1F) | pa);
	}
}

/**
 * Set the output power level of the RFM69 module.
 *
 * @param device rfm69 device
 * @param power Power level from 0 to 31.
 */
void rfm69_setPowerLevel(struct rfm69_dev *device, uint8_t level) {
	if (level > 31)
		level = 31;

	rfm69_writeRegister(device, 0x11,
			(rfm69_readRegister(device, 0x11) & 0xE0) | level);
	device->power_level = level;
}

/**
 * Enable the +20 dBm high power settings of RFM69Hxx modules.
 *
 * @note Enabling only works with high power devices.
 *
 * @param device rfm69 device
 * @param enable true or false
 */
void rfm69_setHighPowerSettings(struct rfm69_dev *device, bool enable) {
	// enabling only works if this is a high power device
	if (enable && !device->is_high_power)
		enable = 0;

	rfm69_writeRegister(device, 0x5A, enable ? 0x5D : 0x55);
	rfm69_writeRegister(device, 0x5C, enable ? 0x7C : 0x70);
	device->highPowerSettings = enable;
}

/**
 * send a packet over the air.
 *
 * After sending the packet, the module goes to standby mode.
 * CSMA/CA is used before sending if enabled by function setCSMA() (default: off).
 *
 * @note A maximum amount of RFM69_MAX_PAYLOAD bytes can be sent.
 * @note This function blocks until packet has been sent.
 *
 * @param device
 * @param data Pointer to buffer with data
 * @param dataLength Size of buffer
 *
 * @return Number of bytes that have been sent
 */
int rfm69_send(struct rfm69_dev *device, uint8_t *data, uint8_t dataLength) {
	uint8_t reg = REG_FIFO | 0x80;
	// switch to standby and wait for mode ready, if not in sleep mode
	if (device->mode != RFM69_MODE_SLEEP) {
		rfm69_setMode(device, RFM69_MODE_STANDBY);
		rfm69_waitForModeReady(device);
	}

	if (dataLength == 0)
		return 0;

	rfm69_clear_FIFO(device);

	if (dataLength > RFM69_MAX_PAYLOAD)
		dataLength = RFM69_MAX_PAYLOAD;

	rfm69_wait_for_free_channel(device);

	device->chip_select(true);

	// transfer packet to FIFO

	// address FIFO
	device->write(&reg, 1);
	// Data length
	if (device->packet_format == RFM69_PACKET_LENGTH_VARIABLE) {
		dataLength++;
		device->write(&dataLength, 1);
	}

	// Data
	device->write(data, dataLength);
	device->chip_select(false);

	if (device->int_packet_sent)
		return dataLength;
	rfm69_setMode(device, RFM69_MODE_TX);

	// wait for packet sent
	rfm69_waitForPacketSent(device);

	// go to standby
	rfm69_setMode(device, RFM69_MODE_STANDBY);

	return dataLength;
}

/**
 * Switch the mode of the RFM69 module.
 * Using this function you can manually select the RFM69 mode (sleep for example).
 *
 * This function also takes care of the special registers that need to be set when
 * the RFM69 module is a high power device (RFM69Hxx).
 *
 * This function is usually not needed because the library handles mode changes automatically.
 * @param device
 * @param mode RFM69_MODE_SLEEP, RFM69_MODE_STANDBY, RFM69_MODE_FS, RFM69_MODE_TX, RFM69_MODE_RX
 * @return The new mode
 */
RFM69Mode rfm69_setMode(struct rfm69_dev *device, RFM69Mode mode) {
	if ((mode == device->mode) || (mode > RFM69_MODE_RX))
		return device->mode;

	// set new mode
	rfm69_writeRegister(device, 0x01, mode << 2);

	// set special registers if this is a high power device (RFM69HW)
	if (device->is_high_power) {
		switch (mode) {
		case RFM69_MODE_RX:
			// normal RX mode
			if (device->highPowerSettings)
				rfm69_setHighPowerSettings(device, false);
			break;

		case RFM69_MODE_TX:
			// +20dBm operation on PA_BOOST
			if (device->highPowerSettings)
				rfm69_setHighPowerSettings(device, true);
			break;

		default:
			break;
		}
	}

	device->mode = mode;

	return device->mode;
}

/**
 * Wait for mode to be set
 * @param device
 */
void rfm69_waitForModeReady(struct rfm69_dev *device) {
	uint32_t timeEntry = device->get_tick();

	while (((rfm69_readRegister(device, 0x27) & 0x80) == 0)
			&& ((device->get_tick() - timeEntry) < TIMEOUT_MODE_READY))
		;
}

/**
 * Wait until packet has been sent over the air or timeout
 * @param device
 */
void rfm69_waitForPacketSent(struct rfm69_dev *device) {
	uint32_t timeEntry = device->get_tick();
	while (((rfm69_readRegister(device, 0x28) & 0x08) == 0)
			&& ((device->get_tick() - timeEntry) < TIMEOUT_PACKET_SENT))
		;
}

/**
 * @brief Put the RFM69 module to sleep
 * @param device
 */
void rfm69_sleep(struct rfm69_dev *device) {
	rfm69_setMode(device, RFM69_MODE_SLEEP);
}

/**
 * Set the carrier frequency in Hz.
 * After calling this function, the module is in standby mode.
 *
 * @param device
 * @param frequency Carrier frequency in Hz
 */
void rfm69_setFrequency(struct rfm69_dev *device, uint32_t frequency) {
	// switch to standby if TX/RX was active
	RFM69Mode oldMode = device->mode;
	if (oldMode == RFM69_MODE_TX) {
		rfm69_setMode(device, RFM69_MODE_RX);
	}

	// calculate register value
	frequency /= RFM69_FSTEP;

	// set new frequency
	rfm69_writeRegister(device, 0x07, frequency >> 16);
	rfm69_writeRegister(device, 0x08, frequency >> 8);
	rfm69_writeRegister(device, 0x09, frequency);

	if (oldMode == RFM69_MODE_RX) {
		rfm69_setMode(device, RFM69_MODE_FS);
	}
	rfm69_setMode(device, oldMode);
}

/**
 * Get device frequency
 * @param device
 * @return frequency
 */
uint32_t rfm69_getFrequency(struct rfm69_dev *device) {
	uint32_t freq;
	freq = RFM69_FSTEP
			* (((uint32_t) rfm69_readRegister(device, 0x07) << 16)
					+ ((uint16_t) rfm69_readRegister(device, 0x08) << 8)
					+ rfm69_readRegister(device, 0x09));
	return freq;
}

/**
 * Set the FSK frequency deviation in Hz.
 * After calling this function, the module is in standby mode.
 *
 * @param device
 * @param frequency Frequency deviation in Hz
 */
void rfm69_setFrequencyDeviation(struct rfm69_dev *device, uint32_t frequency) {
	// switch to standby if TX/RX was active
	if (device->mode == RFM69_MODE_RX || device->mode == RFM69_MODE_TX)
		rfm69_setMode(device, RFM69_MODE_STANDBY);

	// calculate register value
	frequency /= RFM69_FSTEP;

	// set new frequency
	rfm69_writeRegister(device, 0x05, frequency >> 8);
	rfm69_writeRegister(device, 0x06, frequency);
}

/**
 * Set the bitrate in bits per second.
 * After calling this function, the module is in standby mode.
 *
 * @param device
 * @param bitrate Bitrate in bits per second
 */
void rfm69_setBitrate(struct rfm69_dev *device, uint32_t bitrate) {
	// switch to standby if TX/RX was active
	if (RFM69_MODE_RX == device->mode || RFM69_MODE_TX == device->mode)
		rfm69_setMode(device, RFM69_MODE_STANDBY);

	// calculate register value
	bitrate = RFM69_FXOSC / bitrate;

	// set new bitrate
	rfm69_writeRegister(device, 0x03, bitrate >> 8);
	rfm69_writeRegister(device, 0x04, bitrate);
}

/**
 * Put the RFM69 module in RX mode and try to receive a packet.
 *
 * @note This is an internal function.
 * @note The module resides in RX mode.
 *
 * @param device
 * @param data Pointer to a receiving buffer
 * @param dataLength Maximum size of buffer
 * @return Number of received bytes; 0 if no payload is available.
 */
int rfm69_receive(struct rfm69_dev *device, uint8_t *data, uint32_t dataLength) {
	// go to RX mode if not already in this mode
	if (RFM69_MODE_RX != device->mode) {
		rfm69_setMode(device, RFM69_MODE_RX);
		rfm69_waitForModeReady(device);
	}

	// check for flag PayloadReady
	if (rfm69_readRegister(device, 0x28) & 0x04) {
		// go to standby before reading data
		rfm69_setMode(device, RFM69_MODE_STANDBY);

		// get FIFO content
		uint32_t bytesRead = 0;

		// read until FIFO is empty or buffer length exceeded
		while ((rfm69_readRegister(device, 0x28) & 0x40)
				&& (bytesRead < dataLength)) {
			// read next byte
			data[bytesRead] = rfm69_readRegister(device, 0x00);
			bytesRead++;
		}

		// go back to RX mode
		rfm69_setMode(device, RFM69_MODE_RX);
		// todo: wait needed?
		//		rfm69_waitForModeReady();

		return bytesRead;
	} else
		return 0;
}

/**
 * @brief Clear FIFO and registers
 * @param device
 */
void rfm69_clear_FIFO(struct rfm69_dev *device) {
	// clear flags and FIFO
	rfm69_writeRegister(device, 0x28, 0x10);
}

/**
 * @brief Set the output power level in dBm
 *
 * This function takes care of the different PA settings of the modules.
 * Depending on the requested power output setting and the available module,
 * PA0, PA1 or PA1+PA2 is enabled.
 *
 * Output power of module is from -18 dBm to +13 dBm
 * in "low" power devices, -2 dBm to +20 dBm in high power devices
 *
 * @param device
 * @param dBm Output power in dBm
 * @return 0 if dBm valid; else -1.
 */
int rfm69_setPowerDBm(struct rfm69_dev *device, int8_t dBm) {
	if (dBm < -18 || dBm > 20)
		return -1;

	if (!device->is_high_power && dBm > 13)
		return -1;

	if (device->is_high_power && dBm < -2)
		return -1;

	uint8_t powerLevel = 0;

	if (!device->is_high_power) {
		// only PA0 can be used
		powerLevel = dBm + 18;

		// enable PA0 only
		rfm69_writeRegister(device, 0x11, 0x80 | powerLevel);
	} else {
		if (dBm >= -2 && dBm <= 13) {
			// use PA1 on pin PA_BOOST
			powerLevel = dBm + 18;

			// enable PA1 only
			rfm69_writeRegister(device, 0x11, 0x40 | powerLevel);

			// disable high power settings
			rfm69_setHighPowerSettings(device, false);
		} else if (dBm > 13 && dBm <= 17) {
			// use PA1 and PA2 combined on pin PA_BOOST
			powerLevel = dBm + 14;

			// enable PA1+PA2
			rfm69_writeRegister(device, 0x11, 0x60 | powerLevel);

			// disable high power settings
			rfm69_setHighPowerSettings(device, false);
		} else {
			// output power from 18 dBm to 20 dBm, use PA1+PA2 with high power settings
			powerLevel = dBm + 11;

			// enable PA1+PA2
			rfm69_writeRegister(device, 0x11, 0x60 | powerLevel);

			// enable high power settings
			rfm69_setHighPowerSettings(device, true);
		}
	}

	return 0;
}

/**
 * Read the last RSSI value.
 *
 * @note Only if the last RSSI value was above the RSSI threshold, a sample can be read.
 *       Otherwise, you always get -127 dBm. Be also careful if you just switched to RX mode.
 *       You may have to wait until a RSSI sample is available.
 *
 * @return RSSI value in dBm.
 */
int rfm69_readRSSI(struct rfm69_dev *device) {
	return rfm69_readRegister(device, 0x24) / 2;
}

/**
 * Enable/disable OOK modulation (On-Off-Keying).
 *
 * Default modulation is FSK.
 * The module is switched to standby mode if RX or TX was active.
 *
 * @param enable true or false
 */
void rfm69_setOOKMode(struct rfm69_dev *device, bool enable) {
	// switch to standby if TX/RX was active
	if (RFM69_MODE_RX == device->mode || RFM69_MODE_TX == device->mode)
		rfm69_setMode(device, RFM69_MODE_STANDBY);

	if (enable) {
		// OOK
		rfm69_writeRegister(device, 0x02,
				(rfm69_readRegister(device, 0x02) & 0xE7) | 0x08);
	} else {
		// FSK
		rfm69_writeRegister(device, 0x02,
				(rfm69_readRegister(device, 0x02) & 0xE7));

	}

	device->OOK_enabled = enable;
}

/**
 * Transmit a high or low bit in continuous mode using the external data line.
 *
 * @note Call setDataMode() before to enable continuous mode.
 *
 * @param bit true: high bit; false: low bit
 */
void rfm69_continuousBit(struct rfm69_dev *device, bool bit) {
	// only allow this in continuous mode and if data pin was specified
	if ((device->data_mode == RFM69_DATA_MODE_PACKET)
			|| (device->datapin_set == 0))
		return;

	// send low or high bit
	device->datapin_set(bit);
}

/**
 * Configure the data mode of the RFM69 module.
 *
 * Default data mode is 'packet'. You can choose between 'packet',
 * 'continuous with clock recovery', 'continuous without clock recovery'.
 *
 * The module is switched to standby mode if RX or TX was active.
 *
 * @param dataMode RFM69_DATA_MODE_PACKET, RFM69_DATA_MODE_CONTINUOUS_WITH_SYNC, RFM69_DATA_MODE_CONTINUOUS_WITHOUT_SYNC
 */
void rfm69_setDataMode(struct rfm69_dev *device, RFM69DataMode dataMode) {
	// switch to standby if TX/RX was active
	if (RFM69_MODE_RX == device->mode || RFM69_MODE_TX == device->mode)
		rfm69_setMode(device, RFM69_MODE_STANDBY);

	switch (dataMode) {
	case RFM69_DATA_MODE_PACKET:
		rfm69_writeRegister(device, 0x02,
				(rfm69_readRegister(device, 0x02) & 0x1F));
		break;

	case RFM69_DATA_MODE_CONTINUOUS_WITH_SYNC:
		rfm69_writeRegister(device, 0x02,
				(rfm69_readRegister(device, 0x02) & 0x1F) | 0x40);
		rfm69_writeRegister(device, 0x25, 0x04); // Dio2Mapping = 01 (Data)
		rfm69_continuousBit(device, false);
		break;

	case RFM69_DATA_MODE_CONTINUOUS_WITHOUT_SYNC:
		rfm69_writeRegister(device, 0x02,
				(rfm69_readRegister(device, 0x02) & 0x1F) | 0x60);
		rfm69_writeRegister(device, 0x25, 0x04); // Dio2Mapping = 01 (Data)
		rfm69_continuousBit(device, false);
		break;

	default:
		return;
	}

	device->data_mode = dataMode;
}

bool rfm69_channelFree(struct rfm69_dev *device) {
	if (rfm69_readRSSI(device) < CSMA_RSSI_THRESHOLD) {
		return true;
	} else {
		return false;
	}
}

void rfm69_wait_for_free_channel(struct rfm69_dev *device) {
	if (device->csma_enabled) {
		// Restart RX
		rfm69_writeRegister(device, 0x3D,
				(rfm69_readRegister(device, 0x3D) & 0xFB) | 0x20);

		// switch to RX mode
		rfm69_setMode(device, RFM69_MODE_RX);

		// wait until RSSI sampling is done; otherwise, 0xFF (-127 dBm) is read

		// RSSI sampling phase takes ~960 µs after switch from standby to RX
		uint32_t timeEntry = device->get_tick();
		while (((rfm69_readRegister(device, 0x23) & 0x02) == 0)
				&& ((device->get_tick() - timeEntry) < 10))
			;

		while ((!rfm69_channelFree(device))
				&& ((device->get_tick() - timeEntry) < TIMEOUT_CSMA_READY)) {
			// wait for a random time before checking again
			device->delay_ms(rand() % 10);

		}

		rfm69_setMode(device, RFM69_MODE_STANDBY);
	}
}
