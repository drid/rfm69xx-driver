# RFM69 Driver
by Ilias Daradimos  
RFM69 driver for RFM69W, RFM69HW, RFM69CW, RFM69HCW (semtech SX1231, SX1231H)  
This package contains the RFM69xx radio transceiver driver written in C  
Code is based on https://github.com/ahessling/RFM69-STM32 and https://github.com/LowPowerLab/RFM69 by Felix Rusu

## License
[GPL 3.0](LICENSE), please see the LICENSE file for details. Be sure to include the same license with any fork or redistribution of this library.

## Integration details
* Integrate rfm69.h, rfm69_registers.h and rfm69.c file in to the project.
* Include the rfm69.h file in your code like below.
```c
#include "rfm69.h"
```
## File information

* rfm69_registers.h: This header file has the register constants declerations
* rfm69.h: This header file contains the declarations of the sensor driver APIs
* rfm69.c: This source file contains the definitions of the sensor driver APIs

## Usage guide
### Initializing the transceiver
To initialize the sensor, user need to create a device structure. User can do this by
creating an instance of the structure rfm69_dev. After creating the device strcuture, user
need to fill in the various parameters as shown below

```c
struct rfm69_dev transceiver;

transceiver->read = user_spi_read;
transceiver->write = user_spi_write;
transceiver->chip_reset = user_chip_reset;
transceiver->chip_select = user_chip_select;
transceiver->delay_ms = user_delay_ms;
transceiver->get_tick = user_get_tick;

rfm69_init(&transceiver);

```

### Bulk configuration
Device parameters can either be set from corresponding function calls or they can be set all at once using the device.conf pointer.
This pointer needs to be set to a 2 dimensional array of registers and values.
The number of resisters to be set must be assigned to device.conf_size.
Registers are set when rfm69_init() is called and device.conf_size is not 0

Sample code:
``` c
#define RF_FREQ		433000000U
#define RF_FREQ_DEV	5000U
#define RF_BITRATE	9600

#define RF_FREQ_MSB ((uint32_t)(RF_FREQ/RFM69_FSTEP) & 0xFF0000)>>16
#define RF_FREQ_MID ((uint32_t)(RF_FREQ/RFM69_FSTEP) & 0x00FF00)>>8
#define RF_FREQ_LSB ((uint32_t)(RF_FREQ/RFM69_FSTEP) & 0xFF)

#define RF_FREQ_DEV_MSB ((uint32_t)(RF_FREQ_DEV/RFM69_FSTEP) & 0xFF00)>>8
#define RF_FREQ_DEV_LSB ((uint32_t)(RF_FREQ_DEV/RFM69_FSTEP) & 0xFF)+1

#define RF_BITRATE_MSB ((uint32_t)(RFM69_FXOSC/RF_BITRATE) & 0xFF00)>>8
#define RF_BITRATE_LSB ((uint32_t)(RFM69_FXOSC/RF_BITRATE) & 0xFF)+1

static uint8_t rfm69_base_config[][2] =
{
	    {0x01, 0x00}, // RegOpMode: Sleep Mode
	    {0x02, 0x00}, // RegDataModul: Packet mode, FSK
	    {0x03, RF_BITRATE_MSB}, // RegBitrateMsb: 9.6 kbps
	    {0x04, RF_BITRATE_LSB}, // RegBitrateLsb
	    {0x05, RF_FREQ_DEV_MSB}, // RegFdevMsb: 20 kHz
	    {0x06, RF_FREQ_DEV_LSB}, // RegFdevLsb
	    {0x07, RF_FREQ_MSB}, // RegFrfMsb: 433 MHz
	    {0x08, RF_FREQ_MID}, // RegFrfMid
	    {0x09, RF_FREQ_LSB}, // RegFrfLsb
	    {0x18, 0x88}, // RegLNA: 200 Ohm impedance, gain set by AGC loop
	    {0x19, 0x4C}, // RegRxBw: 25 kHz
	    {0x2C, 0x00}, // RegPreambleMsb: 3 bytes preamble
	    {0x2D, 0x03}, // RegPreambleLsb
	    {0x2E, 0x98}, // RegSyncConfig: Enable sync word, 2 bytes sync word
	    {0x2F, 0x7A}, // RegSyncValue1: 0x7A0E
	    {0x30, 0x0E}, // RegSyncValue2
	    {0x37, 0x18}, // RegPacketConfig1: Fixed length, CRC on, no whitening
	    {0x38, 0x40}, // RegPayloadLength: 64 bytes max payload
        {0x3B, 0x5B}, // RegAutoModes
	    {0x3C, 0x8F}, // RegFifoThresh: TxStart on FifoNotEmpty, 15 bytes FifoLevel
	    {0x58, 0x1B}, // RegTestLna: Normal sensitivity mode
	    {0x6F, 0x30}, // RegTestDagc: Improved margin, use if AfcLowBetaOn=0 (default)
};

transceiver->conf = (uint8_t*)rfm69_base_config;
transceiver->conf_size = sizeof(rfm69_base_config)/2;

rfm69_init(&transceiver);
```
