/**
 * @file rfm69.h
 * @brief RFM69 and RFM69HW library for sending and receiving packets
 *
 * This is a protocol and hardware agnostic driver library for handling HopeRF's RFM69 433/868/915 MHz RF modules.
 * Support is also available for the +20 dBm high power modules called RFM69HW/RFM69HCW.
 *
 * A CSMA/CA (carrier sense multiple access) algorithm can be enabled to avoid collisions.
 * If you want to enable CSMA, you should initialize the random number generator before.
 *
 * This work is based on RFM69-STM32 library by André Heßling https://github.com/ahessling/RFM69-STM32
 *
 * @date Apr 17, 2020
 * @author Ilias Daradimos
 */

#ifndef RFM69_H_
#define RFM69_H_

#include <stdbool.h>
#include <stdint.h>

#define RFM69_MAX_PAYLOAD	64 ///< Maximum bytes payload
#define RFM69_FXOSC         32000000    ///< Internal clock frequency [Hz]
#define RFM69_FSTEP         61.03515625 ///< Step width of synthesizer [Hz]

/**
 * Valid RFM69 operation modes.
 */
typedef enum
{
  RFM69_MODE_SLEEP = 0,//!< Sleep mode (lowest power consumption)
  RFM69_MODE_STANDBY,  //!< Standby mode
  RFM69_MODE_FS,       //!< Frequency synthesizer enabled
  RFM69_MODE_TX,       //!< TX mode (carrier active)
  RFM69_MODE_RX        //!< RX mode
} RFM69Mode;

/**
 * Valid RFM69 data modes.
 */
typedef enum
{
  RFM69_DATA_MODE_PACKET = 0,                 //!< Packet engine active
  RFM69_DATA_MODE_CONTINUOUS_WITH_SYNC = 2,   //!< Continuous mode with clock recovery
  RFM69_DATA_MODE_CONTINUOUS_WITHOUT_SYNC = 3,//!< Continuous mode without clock recovery
} RFM69DataMode;

typedef enum {
	RFM69_PACKET_LENGTH_FIXED,
	RFM69_PACKET_LENGTH_VARIABLE
} RFM69PacketFormat;

/*!
 * @brief Function pointer definitions
 */
typedef uint8_t (*rfm69_spi_fptr_t)(uint8_t *data, unsigned short len);
typedef void (*rfm69_gpio_fptr_t)(uint8_t state);
typedef uint32_t (*rfm69_time_fptr_t)(void);
typedef void (*rfm69_delay_fptr_t)(uint32_t period);
/*!
 * @brief rfm69 device structure
 */
struct rfm69_dev
{
	bool is_high_power;
	bool init;
	RFM69Mode mode;
	RFM69DataMode data_mode;
	uint8_t power_level;
	bool csma_enabled;
	bool highPowerSettings;
	bool OOK_enabled;
	RFM69PacketFormat packet_format;
	bool int_packet_sent;
    /*! CS function pointer */
    rfm69_gpio_fptr_t chip_select;
    /*! Reset function pointer */
    rfm69_gpio_fptr_t chip_reset;
    rfm69_gpio_fptr_t datapin_set;
    /*! Read function pointer */
    rfm69_spi_fptr_t read;
    /*! Write function pointer */
    rfm69_spi_fptr_t write;
    /*! Delay function pointer */
    rfm69_delay_fptr_t delay_ms;
    /*! Get system tick function */
    rfm69_time_fptr_t get_tick;
    uint8_t* conf;
    uint8_t conf_size;
};

#endif /* RFM69_H_ */

int rfm69_receive(struct rfm69_dev *device, uint8_t* data, uint32_t dataLength);
int rfm69_send(struct rfm69_dev *device, uint8_t* data, uint8_t dataLength);
int rfm69_setPowerDBm(struct rfm69_dev *device, int8_t dBm);
RFM69Mode rfm69_setMode(struct rfm69_dev *device, RFM69Mode mode);
uint32_t rfm69_getFrequency();
uint8_t rfm69_init(struct rfm69_dev *device);
void rfm69_reset(struct rfm69_dev *device);
void rfm69_setBitrate(struct rfm69_dev *device, uint32_t bitrate);
void rfm69_setCustomConfig(struct rfm69_dev *device, uint8_t *config, uint8_t length);
void rfm69_setFrequency(struct rfm69_dev *device, uint32_t frequency);
void rfm69_setFrequencyDeviation(struct rfm69_dev *device, uint32_t frequency);
void rfm69_setPowerLevel(struct rfm69_dev *device, uint8_t level);

